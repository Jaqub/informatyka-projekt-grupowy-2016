var renderer;
renderer = new THREE.WebGLRenderer();
		//renderer.setSize( window.innerWidth, window.innerHeight );  //wielkosc okna renderowania
		renderer.setSize( window.innerWidth -16, window.innerHeight - 64 ); //okno renderowania
		renderer.shadowMapEnabled=true;
		renderer.shadowMapType=THREE.PCFSoftShadowMap;

var textures = ['img/bump.jpg'];
var wall = [];
for(var i=0;i<textures.length;i++) {
	var texture = THREE.ImageUtils.loadTexture(textures[i]);
	texture.anisotropy = renderer.getMaxAnisotropy();
	wall.push(    new THREE.MeshBasicMaterial({map: texture}));  //Material jakim pokryjemy sciany
}
var texture_1 = THREE.ImageUtils.loadTexture('img/bump2.jpg');
	var brick = [];
	for(var i=0;i<texture_1.length;i++) {
			var texture = THREE.ImageUtils.loadTexture(texture_1[i]);
			texture.anisotropy = renderer.getMaxAnisotropy();
			brick.push(	new THREE.MeshBasicMaterial({map: texture_1}));
		}

function initCube(x,y,z){
	var geometry = new THREE.CubeGeometry(2,2,2); //Obiekt zawierajacy wierzcholki i sciany szescianu
	var material = new THREE.MeshBasicMaterial(  new THREE.MeshBasicMaterial({map: texture})  ); //Material jakim pokryjemy sciany
	var cube = new THREE.Mesh( geometry, material ); //Utworzenie siatki trojkatow tworzacej prostopadloscian/szescian
	scene.add( cube );  //Dodanie szescianu do sceny
	cube.position.x=x;
	cube.position.y=y;
	cube.position.z=z;
}

function initCubik(x,y,z) {
	var geometry2 = new THREE.CubeGeometry(0.5,0.5,0.5); 
	var material = new THREE.MeshBasicMaterial(  new THREE.MeshBasicMaterial({map: texture_1})  );
	var cubik = new THREE.Mesh( geometry2, material );
	scene.add( cubik );
	cubik.position.x=x;
	cubik.position.y=y;
	cubik.position.z=z;
}

function initW() {
    var dae;
    var url='obj/monster.dae';
    var loader = new THREE.ColladaLoader();
    loader.options.convertUpAxis = true;
    loader.load( url, function ( collada ) {
        dae = collada.scene;
        dae.scale.x = dae.scale.y = dae.scale.z =0.001; //skalowanie obiektu
        dae.updateMatrix();
                   
        dae.traverse( function ( child ) {
            child.castShadow = true;
            child.receiveShadow = false;
        } );
    scene.add( dae ); 
	dae.position.x=-0.5;
	dae.position.y=-1;
	dae.position.z=-0.15;
	dae.rotation.y=-0.8;
	dae.rotation.z=0.6;
    } );
}

function initLight(){
	
}

