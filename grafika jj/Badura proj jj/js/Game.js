function createBricks(){
	var geometry = new THREE.CubeGeometry(8,8,2); //Obiekt zawierajacy wierzcholki i sciany szescianu
	var material = new THREE.MeshBasicMaterial( { color: 0xFF0000 } ); //Mateial jakim pokryjemy sciany (tu kolorujemy)
	var brick1 = new THREE.Mesh( geometry, material ); 
	brick1.position.y +=10
	brick1.position.x -=5
	brick1.callback = function() { initObj( this ); }
	scene.add(brick1);
	objects.push(brick1)
	var brick2 = new THREE.Mesh( geometry, material );
	brick2.position.y +=10
	brick2.position.x +=5.5
	brick2.callback = function() { initObj( this ); }
	scene.add(brick2);
	objects.push(brick2)
	var brick3 = new THREE.Mesh( geometry, material );
	brick3.position.y +=10
	brick3.position.x +=16.5
	brick3.callback = function() { initObj( this ); }
	scene.add(brick3);
	objects.push(brick3)
	var brick4 = new THREE.Mesh( geometry, material );
	brick4.position.x +=16.5
	brick4.callback = function() { initObj( this ); }
	scene.add(brick4);
	objects.push(brick4)
	var brick5 = new THREE.Mesh( geometry, material );
	brick5.position.x +=5.5
	brick5.callback = function() { initObj( this );}
	scene.add(brick5);
	objects.push(brick5)
	var brick6 = new THREE.Mesh( geometry, material );
	brick6.position.x -=5
	brick6.callback = function() { initObj( this ); }
	scene.add(brick6);
	objects.push(brick6)
	var brick7 = new THREE.Mesh( geometry, material );
	brick7.position.y -=10
	brick7.position.x -=5
	brick7.callback = function() { initObj( this ); }
	scene.add(brick7);
	objects.push(brick7)
	var brick8 = new THREE.Mesh( geometry, material );
	brick8.position.y -=10
	brick8.position.x +=5.5
	brick8.callback = function() { initObj( this ); }
	scene.add(brick8);
	objects.push(brick8)
	var brick9 = new THREE.Mesh( geometry, material );
	brick9.position.y -=10
	brick9.position.x +=16.5
	brick9.callback = function() { initObj( this );}
	scene.add(brick9);
	objects.push(brick9)
};

function placeHand(){
	var handGeometry = setObject() //Obiekt zawierajacy wierzcholki i sciany szescianu
	if (handGeometry == null){
		hand = null;
		return;
	}
	var material = setColor()
	hand = new THREE.Mesh( handGeometry, material );
	scene.add(hand)
	hand.position.set(150,0,0)
	var bb = new THREE.Box3()
	bb.setFromObject(hand);
	bb.center(controls.target);
}

function placeHandWithTexture(material){
	var handGeometry = setObject() //Obiekt zawierajacy wierzcholki i sciany szescianu
	if (handGeometry == null){
		hand = null;
		return;
	}
	hand = new THREE.Mesh( handGeometry, material );
	scene.add(hand)
	hand.position.set(150,0,0)
	var bb = new THREE.Box3()
	bb.setFromObject(hand);
	bb.center(controls.target);
}

function setColor(){
	var color =$('#colorPicker').val()
	if(color == '1'){
		return new THREE.MeshBasicMaterial( { color: 0xFF0000 } ); 
	}else if (color == '2'){
		return new THREE.MeshBasicMaterial( { color: 0x0000FF } );
	}else if (color == '3'){
		return new THREE.MeshBasicMaterial( { color: 0x00FF00 } );
	}else{
		return new THREE.MeshNormalMaterial( { color: 0x000000 } );
	}
}

function setObject(){
	
	var objType=$('#objectPicker').val()
	console.log(objType)
	if(objType == '1'){
		return new THREE.CubeGeometry(10,10,10);
	}else if (objType == '2'){
		console.log('test')
		var geometry2 = new THREE.Geometry();

				geometry2.vertices.push( new THREE.Vector3( -5,  -5, -5 ) ); 
				geometry2.vertices.push( new THREE.Vector3( -5, -5, 5 ) );
				geometry2.vertices.push( new THREE.Vector3(  5, -5, 5 ) );
				geometry2.vertices.push( new THREE.Vector3(  5, -5, -5 ) );
				geometry2.vertices.push( new THREE.Vector3(  0, 5, 0 ) );

				geometry2.faces.push( new THREE.Face3( 0, 1, 4 ) );		
				geometry2.faces.push( new THREE.Face3( 1, 2, 4 ) );
				geometry2.faces.push( new THREE.Face3( 2, 3, 4 ) );
				geometry2.faces.push( new THREE.Face3( 3, 0, 4 ) );
		return geometry2;
	}else if (objType == '3'){
		return  new THREE.SphereGeometry(5, 50, 50, 0, Math.PI * 2, 0, Math.PI * 2);
	}else if (objType == '4'){
		initCollada('objects/monster.dae.xml')
		return  null;
	}else{
		return new THREE.CubeGeometry(10,10,10);
	}
}

function createMap(){
	var geometry = new THREE.CubeGeometry(1,30,1); 
			var material = new THREE.MeshBasicMaterial( { color: 0xFFFF00 } ); 
			var cube1 = new THREE.Mesh( geometry, material ); 
			var cube2 = new THREE.Mesh( geometry, material );
			focusCube = new THREE.Mesh( geometry, material );
			var cube4 = new THREE.Mesh( geometry, material );
			scene.add( cube1 );  //Dodanie szescianu do sceny
			scene.add( cube2 );
			scene.add( focusCube );
			scene.add( cube4 );
			
			cube2.position.x =11
			camera.position.z = 30; // przesuniecie kamery
			
			focusCube.rotation.z = Math.PI / 2;
			focusCube.position.y =5
			focusCube.position.x =5
			
			cube4.rotation.z = Math.PI / 2;
			cube4.position.y -=5
			cube4.position.x =5
}	

function restartGame(){
	console.log('testowo')
	for(var x =0;x<objects.length;x++){
		scene.remove(objects[x]);
	}
	for(var y =0;x<crossCircleTab.length;x++){
		scene.remove(crossCircleTab[x]);
	}
	crossCircleTab=[];
	objects=[];
	createBricks()
}
function initCollada(url) {
	var loader = new THREE.ColladaLoader();
	loader.options.convertUpAxis = true;
	loader.load( url, function ( collada ) {
		dae = collada.scene;
		

    // Create animation action and start it
		
		
		dae.scale.x = dae.scale.y = dae.scale.z = 0.01; //skalowanie obiektu
		dae.updateMatrix();
					
		dae.traverse( function ( child ) {
			child.castShadow = true;
			child.receiveShadow = false;
			
		} ); 
	scene.add( dae );
	coladaStorage.push(dae)
	} );
	
}

function initObj(obj) {
	pickedMesh=obj
	window.setTimeout(function(){
		if(decision==true){
			var loader = new THREE.JSONLoader();
			loader.load('objects/cross.json', function(geometry,materials) {
			var material = new THREE.MeshFaceMaterial( materials );
				mesh = new THREE.Mesh(geometry,material);
				mesh.scale.x = mesh.scale.y = mesh.scale.z = 2
				scene.add(mesh);
				
				mesh.position.x=obj.position.x
				mesh.position.y=obj.position.y
				
				mesh.rotation.y = Math.PI / 2;
				scene.remove(obj)
				decision = false
				crossCircleTab.push(mesh)
			});
		}else{
			var loader = new THREE.JSONLoader();
			loader.load('objects/ring.json', function(geometry,materials) {
			var material = new THREE.MeshFaceMaterial( materials );
				mesh = new THREE.Mesh(geometry,material);
				mesh.scale.x = mesh.scale.y = mesh.scale.z = 2
				scene.add(mesh);
				
				mesh.position.x=obj.position.x+2
				mesh.position.y=obj.position.y-2
				
				mesh.rotation.y = Math.PI / 2;
				scene.remove(obj)
				decision= true
				crossCircleTab.push(mesh)
			});
		}
		pickedMesh = null;
	}, 5000)	
}		

function onDocumentMouseDown( event ) {
	event.preventDefault();
	mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;

	raycaster.setFromCamera( mouse, camera );

	var intersects = raycaster.intersectObjects( objects ); 
	if ( intersects.length > 0 ) {
		var tempObjects = []
		for(var x =0;x<objects.length;x++){
			
			if(objects[x]!= intersects[0].object){
				tempObjects.push(objects[x])
			}
		}
		objects=tempObjects;
		intersects[0].object.callback();
	}
}



function initGround(){
	var groundTexture = loader.load( 'img/grasslight-big.jpg' );
	groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
	groundTexture.repeat.set( 25, 25 );
	groundTexture.anisotropy = 16;
	var groundMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x111111, map: groundTexture } );
	var mesh = new THREE.Mesh( new THREE.PlaneBufferGeometry( 20000, 20000 ), groundMaterial );
	mesh.rotation.x = - Math.PI / 2;
	mesh.position.y -=30;
	scene.add( mesh );
}

function addTexture(url){
	var groundTexture = loader.load(url);
	groundTexture.wrapS = groundTexture.wrapT = THREE.RepeatWrapping;
	groundTexture.repeat.set( 1, 1 );
	groundTexture.anisotropy = 16;
	var groundMaterial = new THREE.MeshPhongMaterial( { color: 0xffffff, specular: 0x111111, map: groundTexture } );
	var positionX = hand.position.x;
	var positionY = hand.position.y;
	var positionZ = hand.position.z;
	scene.remove(hand)
	placeHandWithTexture(groundMaterial)
	hand.position.x = positionX
	hand.position.z = positionZ
	hand.position.y = positionY
}






